local component = require("component")
local sides = require("sides")
local xnet = component.xnet
local serialization = require("serialization")

local test_block = {}
function lookup(t, ...)
    for _, k in ipairs{...} do
        t = t[k]
        if not t then
            return nil
        end
    end
    return t
end
for i,block in ipairs(xnet.getConnectedBlocks()) do
    print(i,serialization.serialize(block))
    if block.connector == "squeezer" then
        print("found squeezer")
        table.insert(test_block,block)
        capabliities = xnet.getSupportedCapabilities(block.pos)
        for j, cap in ipairs(capabliities) do
            print(j, cap)
        end
    end
end
print()


-- for i in xnet.getItems(laser.pos) do
--     xnet.transferItem(laser.pos, i.)
-- Catelog current ores
-- debug.setmetatable(storage, {__index = false})
print(serialization.serialize(test_block))
if lookup(test_block, "connector") then
    print("not nil")
else
    print("nil")
end
for i,storage in ipairs(test_block) do
    print(storage.connector)
    local items = xnet.getItems(storage.pos, sides.down)
    print(serialization.serialize(items))
    if items ~= nil then
        print(#items)
        for a,item in ipairs(items) do
            print(a,item.label,item.size,item.name)
        end
    else
        print("items is nil")
    end
end