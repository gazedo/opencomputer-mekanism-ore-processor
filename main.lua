package.loaded["lib/storage"] = nil
package.loaded["lib/machines"] = nil
local storage = require('lib/storage')
local machine = require('lib/machines')
local serialization = require("serialization")
local os = require('os')
local json = require('lib/json/json')
local sides = require('sides')

-- init machines
    -- clear current lenses
local raw_source, raw_buffer, squeezer, pulverizer, enrich, full, final,done = storage.find_network()
print(serialization.serialize(final))
print()
local process_blocks = {
    raw_source = machine:init(raw_source, raw_buffer, sides.front, sides.front, 0,0),
    full = machine:init(full, {done}, sides.top, sides.right, 3, 117),
    squeezer = machine:init(squeezer, squeezer, sides.top, sides.bottom, 1, 4),
    pulverizer = machine:init(pulverizer, pulverizer, sides.top, sides.bottom, 1, 2),
    enrich = machine:init(enrich, enrich, sides.left, sides.back, 9, 117),
    final = machine.init({final}, {final}, sides.back, sides.back, 0,0)
}
f = io.open("lib/oreList.json")
file = f:read("*all")
f:close()
local ore_dict = json.decode(file)
-- print(serialization.serialize(process_blocks.raw_source))
-- print(ore_dict["Quartz Ore"].Processing)


-- screen:init()
-- init priority list
    -- read json
    -- check current levels
-- storage:update_priorities()
-- check available lens
    -- read lens storage and save table
while true do
    -- print("moving from raw_source to raw_buffer")
    storage.move_item_to(process_blocks.raw_source:take(), raw_buffer.pos, sides.back)
    -- print("moving from squeezer to final")
    storage.move_item_to(process_blocks.squeezer:take(), done.pos, sides.back)
    -- print("moving from pulverizer to final")
    storage.move_item_to(process_blocks.pulverizer:take(), done.pos, sides.back)
    -- print("moving from raw_source to processing blocks")
    storage.process(raw_buffer.pos, ore_dict, process_blocks)
    -- print("moving from done to final")
    storage.move_item_to({pos = done.pos, sides.back}, final.pos, sides.back)
    -- storage.count()
    os.sleep(0.05)
end
