local component = require("component")
local serialization = require("serialization")
local xnet = component.xnet
local sides = require("sides")
local table = require("table")
local utils = require("lib/utils")
local storage = {}

function storage.find_network()
    local raw_source, squeezer, pulverizer, enrich, full = {}, {}, {}, {}, {}
    local raw_buffer, final, done
    -- local raw_source = {}
    for i,block in ipairs(xnet.getConnectedBlocks()) do
        if block.connector == "raw_source" then
            print("Found raw_source")
            table.insert(raw_source,block)
        elseif block.connector == "raw_buffer" then
            print("Found raw_buffer")
            raw_buffer = block
        elseif block.connector == "squeezer" then
            print("Found squeezer")
            table.insert(squeezer,block)
        elseif block.connector == "pulverizer" then
            print("Found pulverizer")
            table.insert(pulverizer,block)
        elseif block.connector == "enrich" then
            print("Found enrich")
            table.insert(enrich,block)
        elseif block.connector == "full" then
            print("Found full")
            table.insert(full, block)
        elseif block.connector == "final" then
            print("Found final")
            final = block
        elseif block.connector == "done" then
            print("Found done")
            done = block
        end
    end
    assert(#raw_source>0, "Unable to find raw_source xnet connector")
    assert(#squeezer>0, "Unable to find squeezer xnet connector")
    assert(#pulverizer>0, "Unable to find pulverizer xnet connector")
    assert(#enrich>0, "Unable to find enrich xnet connector")
    assert(#full>0, "Unable to find enrich xnet connector")
    if raw_buffer == nil then
        error("Unable to find raw_buffer xnet connector")
    end
    if final == nil then
        error("Unable to find final xnet connector")
    end
    return raw_source, raw_buffer, squeezer, pulverizer, enrich, full, final,done
end

function transfer(source_pos, source_side, destination_pos, destination_side)
    for i,item in ipairs(xnet.getItems(source_pos, source_side)) do
        if item.size ~= nil and item.size > 0 then
            -- print("Found Item in slot " .. i .. ", transferring to " .. serialization.serialize(destination_pos) .. ":")
            -- print("\t" .. serialization.serialize(item))
            local result = xnet.transferItem(source_pos, i, item.size, destination_pos, source_side, destination_side)
            if result ~= nil then
                -- print("Items transfered " .. result .. " out of " .. item.size)
            else
                -- print("\027[31mItem transfer from slot " .. i .. " failed\027[0m")
            end
        end
    end
end

-- source and destination are lists that look like {pos = {{x=1.0,y=1.0,z=1.0}}, side = sides.front}
-- with pos being either a single item pos or multiple so we need to check
function storage.move_item_to(source, destination_pos, destination_side)
    if source.pos.x ~= nil then
        -- then this not is a list of positions
        transfer(source.pos, source.side, destination_pos, destination_side)
    else
        for j, block_pos in ipairs(source.pos) do
            transfer(block_pos, source.side, destination_pos, destination_side)
        end
    end
end

function storage.process(raw_buffer_pos,ore_dict, process_machines)
    local result
    for i,item in ipairs(xnet.getItems(raw_buffer_pos)) do 
        if ore_dict[item.label] ~= nil then
            local destination = ore_dict[item.label].Processing
            local target = process_machines[destination]:put()
            -- print("Found " .. item.label .. " moving to " .. destination .. " which should be at " .. serialization.serialize(target))
            -- result = storage.move_single_item_to({pos = raw_buffer_pos,side = sides.front}, target.pos, target.side, i)
            result = xnet.transferItem(raw_buffer_pos, i, item.size, target.pos, sides.front, target.side)
        elseif item.size ~= nil then
            print("Didn't find " .. serialization.serialize(item))
            -- result = storage.move_to(block, process_blocks["final"])
        end
    end
end
return storage
