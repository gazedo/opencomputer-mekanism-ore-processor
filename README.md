# OpenComputer Mekanism Ore Processor

Read from json the desired minimum amount of each ore
count current amounts
check for ores below absolute minimum amount
change lens to maximize highest priority ore
start routing ore through processing chain
move to lower priorities once higher have reached desired levels

Display counts on screen with charts


# Machines
raw source
pulverizer
mechanical squeezer
ore processing chain
enrichment chamber


# Functions
- Read JSON
- Find all machines and save

- get from raw sources and move to buffer
- get from squeezer and move to main storage
- get from pulverizer and move to main storage
- get from final processing buffer and move to main storage 
- Load balancing for multiple processing lines

# Todo
- add logic to skip lens slots in laser base
- count raw and in main storage
- count lenses
- change lenses if available based on what is low
- update screen count


