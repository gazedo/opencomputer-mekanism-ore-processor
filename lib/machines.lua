local serialization =  require("serialization")
local machines = {}
function machines:init(block_in, block_out, side_in, side_out, slots_in, slots_out)
    print(serialization.serialize(block_in))
    print(serialization.serialize(block_out))
    local this = {}
    setmetatable(this, machines)
    this.inPos = {}
    this.outPos = {}
    print("init a machine")
    for i, block in ipairs(block_in) do
        this.inPos[i] = block.pos
        this.outPos[i] = block.pos
    end
    -- for i, block in ipairs(block_out) do
    -- end
    this.side_in = side_in
    this.side_out = side_out
    this.slots_in = slots_in
    this.slots_out = slots_out

    function this:load_balance()
        -- Check current instances group of machines to find the least used one.
        if #this.inPos == 1 then
            return this.inPos[1]
        end
        local item_num = {}
        for i,pos in this.inPos do
            item_num[i] = 0
            for b, item in ipairs(xnet.getItems(pos, this.side_in)) do
                if item.size ~= nil then
                    item_num[i] = item_num[i] + item.size
                end
            end
        end
        local min_value, min_machine = 0,0
        for i, j in ipairs(item_num) do
            if j < min_value then
                min_value = j
                min_key = i
            end
        end
        return this.inPos[min_machine]
    end
    -- just get the list of positions, assumed to be used for an input to move_to
    function this:put()
        -- will return a single location to put into
        return {["pos"] = this.load_balance(), ["side"] = this.side_in}
    end
    function this:take()
        -- will return a list of all locations to take from
        return {["pos"] = this.outPos, ["side"] = this.side_out}
    end
    return this
end

return machines